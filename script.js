/* 1. Чому для роботи з input не рекомендується використовувати клавіатуру?

Тому що подія input спрацьовує щоразу при зміні значення.

На відміну від подій клавіатури, воно працює за будь-яких змін значень, навіть якщо вони не пов'язані з клавіатурними діями: 
вставка за допомогою миші або розпізнавання мови під час диктування тексту.

*/

const chengWords = () => {
  const checkW = document.querySelectorAll('.btn');
  document.addEventListener('keydown', function (event) {
    console.log(event.code);
    console.log(event.key);
    const keyBoard = event.key.toLocaleUpperCase();
    checkW.forEach((item) => {
      const txLocal = item.textContent.toLocaleUpperCase();
      if (keyBoard === txLocal) {
        item.style.backgroundColor = '#0000FF';
      } else {
        item.style.backgroundColor = '#000000';
      }
    });
  });
};
chengWords();
